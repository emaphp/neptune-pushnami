const os = require('os');

const ipAddress = (req) => req.headers['x-forwarded-for'] || req.connection.remoteAddress;

// deprecated
const getLocalExternalIp = () => [].concat.apply([], Object.values(os.networkInterfaces()))
    .filter(details => details.family === 'IPv4' && !details.internal)
    .pop().address;

module.exports = ipAddress;