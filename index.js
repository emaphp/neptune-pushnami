const express = require('express');
const app = express();
const hbs = require('express-handlebars');
const geoip = require('geoip-lite');
const http = require('http');
const getIp = require('./getip');

// public dir
app.use(express.static('public'));

// view engine setup
app.set('view engine', 'handlebars');

app.engine('handlebars', hbs({
  layoutsDir: __dirname + '/views/layouts/',
}));

const VERSION = '1.4';
const DEFAULT_CITY = 'Your City';
// const DEFAULT_CITY = 'Washington, DC';

// Returns an array containing the city and state corresponding to the given timezone
const getDefaultState = timezone => {
  let city = timezone.substr(timezone.indexOf('/') + 1).replace(/_/g, ' ');

  // Some timezones include the state in their names
  // Example: America/North_Dakota/Beulah
  if (city.indexOf('/') !== -1) {
    let state = city.substr(0, city.indexOf('/'));
    city = city.substr(city.indexOf('/') + 1);

    if (state === 'Indiana') {
      return [city, 'IN'];
    } else if (state === 'Kentucky') {
      return [city, 'KY'];
    } else if (state === 'North Dakota') {
      return [city, 'ND'];
    }
  }

  const states = {
    'Adak': 'AK', // Alaska
    'Anchorage': 'AK', // Alaska
    'Juneau': 'AK', // Alaska
    'Metlakatla': 'AK', // Alaska
    'Nome': 'AK', // Alaska
    'Sitka': 'AK', // Alaska
    'Yakutat': 'AK', // Alaska
    'Boise': 'ID', // Idaho
    'New York': 'NY', // New York
    'Chicago': 'IL', // Illinois
    'Denver': 'CO', // Colorado
    'Detroit': 'MI', // Michigan
    'Los Angeles': 'CA', // California
    'Menominee': 'WI', // Wisconsin
    'Phoenix': 'AZ', // Arizona
    'Honolulu': 'HI', // Hawaii
  };

  if (states[city]) {
    return [city, states[city]];
  }

  return [false, false];
};

// Returns the city and region of the current user as a string
const getCity = ipAddress => {
  const geo = geoip.lookup(ipAddress);

  if (!geo) {
    return DEFAULT_CITY;
  }

  // Try to retrieve the city from the timezone
  if (!geo.city) {
    const tz = geo.timezone;
    if (!tz) {
      return DEFAULT_CITY;
    }
    const [city] = getDefaultState(tz);
    if (!city) {
      return DEFAULT_CITY;
    }
    return city;
  }

  return `${geo.city}`;
};

// Returns the city and region of the current user as a string
const getCityRegion = ipAddress => {
  const geo = geoip.lookup(ipAddress);

  if (!geo) {
    return DEFAULT_CITY;
  }

  // Try to retrieve the city from the timezone
  if (!geo.city) {
    const tz = geo.timezone;
    if (!tz) {
      return DEFAULT_CITY;
    }
    const [city, state] = getDefaultState(tz);
    if (!city) {
      return DEFAULT_CITY;
    }
    return `${city}, ${state}`;
  }

  return `${geo.city},${geo.region}`;
};

// Main route
app.get('/', (req, res) => {
  const ipAddress = getIp(req);
  const city = getCity(ipAddress);
  const cityRegion = getCityRegion(ipAddress);

  res.render('main', {
    layout: 'index',
    city,
    cityRegion,
    basePath: process.env.BASE_PATH || '/',
    version: VERSION
  });
});

// Option selector handler
app.get('/submit', (req, res) => {
  const option = req.query.option;
  const source = req.query.source || 'push';
  const site = req.query.site || 'jobezo.com';
  const ipAddress = getIp(req);
  console.log(`Received request from ${getIp(req)}`);

  // Get region info
  const cityRegion = getCityRegion(ipAddress);

  // Couldn't retrieve city/region from user IP
  if (cityRegion === DEFAULT_CITY) {
    res.status(400).redirect(process.env.BASE_PATH || '/');
    return;
  }

  // Redirect
  const url = `http://emailcontent.restorationmedia.net/render-pushnami-notification?site=${site}&job_direct=true&kw=${option}&loc=${cityRegion}&source=${source}&pushnamiSubscriberId=1`;
  console.log(`Retrieving data from url ${url}...`);

  http.get(url, (rres) => {
    let rawData = '';
    rres.on('data', (chunk) => {
      rawData += chunk;
    });
    rres.on('end', () => {
      try {
        const parsedData = JSON.parse(rawData);
        console.log(parsedData);
        res.redirect(parsedData.url);
      } catch (e) {
        console.error(e.message);
        res.status(400).redirect(process.env.BASE_PATH || '/');
      }
    });
  });
});

// console.log(getDefaultState('America/Nome'));
// console.log(getDefaultState('America/New_York'));
// console.log(getDefaultState('America/Indiana/Indianapolis'));
// console.log(getDefaultState('America/North_Dakota/Center'));
// console.log(getDefaultState('America/North_Dakota/New_Salem'));
// console.log(getDefaultState('	Pacific/Honolulu'));

const hostname = 'localhost';
const port = process.env.PORT || 8080;
app.listen(port, hostname, () => {
  console.log("Running on port " + port);
});