(function($) {
  var $searchForm = $('#search-form');
  var $inputJob = $('#input-job');
  var $inputCity = $('#input-city');
  var $inputCityHidden = $('#input-city-hidden');
  var $formSubmit = $('#input-search');

  function parseUrlArgs() {
    var href = location.href;
    var idx = href.indexOf('?');

    if (idx === -1) {
      return false;
    }

    var args = location.href.substr(idx + 1);
    if (args.length === 0) {
      return false;
    }

    var values = {};
    const params = args.split('&');
    params.forEach(function(param) {
      var expr = param.split('=');
      if (expr.length < 2) {
        return;
      }

      if (expr[0] === 'site') {
        values.site = expr[1];
      } else if (expr[0] === 'source') {
        values.source = expr[1];
      }
    });

    return values;
  }

  function submitData(url) {
    // Obtain URL to redirect
    $.get(url, function(response) {
      var redirectTo = response.url;
      if (!redirectTo) {
        alert('Service unavailable');
        $formSubmit.removeAttr('disabled');
        return;
      }

      $formSubmit.removeAttr('disabled');
      window.open(redirectTo, '_blank');
    });
  }

  function buildEndpointArgs(job, city) {
    var values = $.extend({
      site: 'jobezo.com',
      source: 'push',
      kw: job.toLowerCase(),
      loc: city.toLowerCase(),
      job_direct: 'true',
      pushnamiSubscriberId: 1
    }, parseUrlArgs());
    return $.param(values);
  }

  $('a.job-link').click(function() {
    var $el = $(this);
    var job = $el.data('job');
    var city = $inputCityHidden.val();

    gtag('event', 'feed_click', {
      'event_category': 'Click',
      'event_label': job
    });

    var args = buildEndpointArgs(job, city);
    var url = 'http://emailcontent.restorationmedia.net/render-pushnami-notification?' + args;
    submitData(url);
    return false;
  });

  $searchForm.submit(function(e) {
    e.preventDefault();
    $formSubmit.attr('disabled', 'disabled');

    var job = $inputJob.val(),
      city = $inputCity.val();

    gtag('event', 'feed_click', {
      'event_category': 'Click',
      'event_label': job
    });

    var args = buildEndpointArgs(job, city);
    var url = 'http://emailcontent.restorationmedia.net/render-pushnami-notification?' + args;
    submitData(url);
    return false;
  });

  // Keep the form disabled if fields are empty
  function handleTextInput() {
    return function() {
      var job = $inputJob.val(),
        city = $inputCity.val();

      if (job !== "" && city !== "") {
        $formSubmit.removeAttr('disabled');
      } else {
        $formSubmit.attr('disabled', 'disabled');
      }
    };
  }

  $inputJob.keyup(handleTextInput());
  $inputCity.keyup(handleTextInput());
})(jQuery);